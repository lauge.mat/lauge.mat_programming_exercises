"""Chapter 5

Exercise 1 & 2: Write a program which repeatedly reads numbers until the
user enters “done”. Once “done” is entered, print out the total, count,
and average of the numbers. If the user enters anything other than a
number, detect their mistake using try and except and print an error
message and skip to the next number."""

Numbers = []
while True:
        line = input("input a number\n")
        if line == "done":  # if the done command is input this runs    
            if len(Numbers) == 0  # checks if the Numbers list contains at least 1 input to avoid divition error later
                print("plz input a least 1 value")
            else:
                break
        else:
                try:
                        Number = float(line)    # converts to float
                        Numbers.append(Number)  # adds the user inputs to the list Numbers      
                except ValueError:              # checks for non value inputs
                        print("plz input a value :)")

                        
print(Numbers)
total = sum(Numbers)
amount = len(Numbers)
average = total/amount
print(f"the sum is {total}")
print(f"the amount of numbers is {amount}")
print(f"the average is {average}")            
print(f"the minimum is {min(Numbers)}")
print(f"the maximum is {max(Numbers)}")      

input()

