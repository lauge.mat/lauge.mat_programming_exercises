#Calculator
import math
import time

print("welcome to python calculator,\n at any time type done to close the application")

def checker ():
    check = True
    while check:
        value = input("input a number\n")
        if value  == "done":
            exit()
        else:
            try:
                number = float(value)
                check = False
            except ValueError:
                print("plz input a real number")
    return number   
        
V1 = checker()

check = True
while check:
    operator = input("select operator: 1 is addition, 2 is substraction, 3 is multiplication,\n 4 is division, 5 power function and 6 square root\n")
    if operator  == "done":
        print("goodbye")
        exit()
    try:
        op = int(operator)
        if V1 == 42 and op == 42:
            while check:
                print("is the answer "*4)
                time.sleep(0.5)              
        if 1 > op or op > 6:
            print("plz choose a number between 1 and 6")
        else:
            check = False
    except ValueError:
            print("plz input a whole number")                

if op == 1:
    V2 = checker()
    print("sum is " + str(V1+V2))
			
elif op == 2:
    V2 = checker()
    print("difference is " + str(V1-V2))        

elif op == 3:
    V2 = checker()
    print("the product is " + str(V1*V2))

elif op == 4:
    V2 = checker()
    while V2 == 0:
        print("cannot divide by zero")
        V2 = checker()
    print(f"the value is {V1/V2}")
elif op == 5:
    V2 = checker()
    print(f"the value is {V1**V2}")
else:
    while V1 < 0:
        print("imaginary numbers not implemented... yet\ninput a positive value")
        V1 = checker()
    print(f"the square root is {math.sqrt(V1)}")
    
input()        
