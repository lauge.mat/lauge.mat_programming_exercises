""" Ch7

Exercise 2: Exercise 3: Sometimes when programmers get bored or want to have a
bit of fun, they add a harmless Easter Egg to their program. Modify
the program that prompts the user for the file name so that it prints a
funny message when the user types in the exact file name “na na boo
boo”. The program should behave normally for all other files which
exist and don’t exist. Here is a sample execution of the program: """
value = 0
count = 0
file= (input("plz input file name\n"))
if file == "na na boo boo":
    print('NA NA BOO BOO TO YOU - You have been punked!')
    exit()
else:
    file = open(file)
    for line in file:
        if line.startswith('X-DSPAM-Confidence'):
            index = line.find(":")
            line = line[index +1:]
            line = line.strip()
            value = value + float(line)
            count += 1
print(f"the total spam confidence is: {value}")
print(f"the average spam confidence is: {value/count}")
print(count)
input()
