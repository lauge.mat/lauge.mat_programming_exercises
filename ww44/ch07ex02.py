""" Ch7

Exercise 2: Write a program to prompt for a file name, and then read
through the file and look for lines of the form:
X-DSPAM-Confidence: 0.8475
When you encounter a line that starts with “X-DSPAM-Confidence:”
pull apart the line to extract the floating-point number on the line.
Count these lines and then compute the total of the spam confidence
values from these lines. When you reach the end of the file, print out
the average spam confidence. """
value = 0
count = 0
file= open(input("plz input file name\n"))
for line in file:
    if line.startswith('X-DSPAM-Confidence'):
        index = line.find(":")
        line = line[index +1:]
        line = line.strip()
        value = value + float(line)
        count += 1
        #print(line)
print(f"the total spam confidence is: {value}")
print(f"the average spam confidence is: {value/count}")
print(count)
input()
