#Chapter 4

"""Exercise 7: Rewrite the grade program from the previous chapter using
a function called computegrade that takes a score as its parameter and
returns a grade as a string."""

check = bool
while check:
        try:
                score= float(input("what is your score, input between 0-1?\n")) #asks for user input
                if(score > 1.0 or score < 0):
                        print("out of bounds")
                else:
                        check = False
        except ValueError:   # if a non number is input this script runs
                print("Non number Score/Bad score")       

def computegrade(score):
    if score >= 0.9:
        grade="A"
    elif score >= 0.8:
        grade="B"
    elif score >= 0.7:
        grade="C"
    elif score >= 0.6:
        grade="D"
    else: 
        grade="F"

    print("Your Grade is: \n"+str(grade))

computegrade(score) 
input()
