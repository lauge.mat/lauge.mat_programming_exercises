# Chapter 4

"""Exercise 2: Move the last line of this program to the top, so the function
call appears before the definitions. Run the program and see what error
message you get."""

repeat_lyrics()

def print_lyrics():
	print("I'm a lumberjack, and I'm okay.")
	print('I sleep all night and I work all day.')

def repeat_lyrics():
	print_lyrics()
	print_lyrics()
    
"""Traceback (most recent call last):
  File "C:\Users\Administrator\Documents\lauge_mathiesen_programming_exercises\lauge.mat_programming_exercises\ww40\Ch04_02.py", line 7, in <module>
    repeat_lyrics()
NameError: name 'repeat_lyrics' is not defined"""
# funktionen fungerer ikke da den ikke er defineret endnu
